Module	Section	Sample Name	Source
Cutadapt	all_sections	raw.split.reference3_R2	/data/dir0/QC/cutadapt/raw.split.reference3.cutadapt.log
FastQC	all_sections	raw.split.reference3_R2	/data/dir0/QC/fastqc/raw.split.reference3_R2_fastqc.zip
FastQC	all_sections	raw.split.reference3_R1	/data/dir0/QC/fastqc/raw.split.reference3_R1_fastqc.zip
fastp	all_sections	raw.split.reference3_R1.cutadapt	/data/dir0/QC/fastp/raw.split.reference3_fastp.json
